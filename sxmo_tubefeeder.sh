#!/usr/bin/env sh


# shellcheck disable=SC1090
. "$(which sxmo_common.sh)"
# shellcheck disable=SC1090
. "$(which sxmo_icons.sh)"

prompt() {
	sxmo_dmenu.sh -i "$@"
}

output=`~/.local/bin/tubefeeder_cli`

url=`printf "%s\n" "$output" | prompt -p "Video" | cut -f3 -d$'\t'` 

if [ ! -z "${url}" ]; then
    flatpak run com.github.rafostar.Clapper $url &
fi
