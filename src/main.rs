use std::sync::{Arc, Mutex};

use csv_file_manager::CsvFileManager;
use tf_core::{ErrorStore, Generator, Video};
use tf_filter::FilterEvent;
use tf_join::{AnyVideoFilter, SubscriptionEvent};
use tf_observer::{Observable, Observer};

mod csv_file_manager;

#[tokio::main]
async fn main() {
    env_logger::init();

    let joiner = tf_join::Joiner::new();

    let mut subscription_list = joiner.subscription_list();

    let mut user_data_dir =
        std::path::PathBuf::from(&std::env::var("XDG_DATA_DIR").unwrap_or(
            std::env::var("HOME").expect("Failed to get user data dir") + "/.local/share",
        ));
    user_data_dir.push("tubefeeder");

    let mut subscriptions_file_path = user_data_dir.clone();
    subscriptions_file_path.push("subscriptions.csv");

    let _subscription_file_manager = Arc::new(Mutex::new(Box::new(CsvFileManager::new(
        &subscriptions_file_path,
        &mut |sub| subscription_list.add(sub),
    ))
        as Box<dyn Observer<SubscriptionEvent> + Send>));

    subscription_list.attach(Arc::downgrade(&_subscription_file_manager));

    let filters = joiner.filters();
    let mut filters_file_path = user_data_dir.clone();
    filters_file_path.push("filters.csv");

    let _filter_file_manager =
        Arc::new(Mutex::new(
            Box::new(CsvFileManager::new(&filters_file_path, &mut |filter| {
                filters
                    .lock()
                    .expect("Filter Group to be lockable")
                    .add(filter)
            })) as Box<dyn Observer<FilterEvent<AnyVideoFilter>> + Send>,
        ));

    filters
        .lock()
        .expect("Filter Group to be lockable")
        .attach(Arc::downgrade(&_filter_file_manager));

    let error_store = ErrorStore::default();
    let videos = joiner.generate(&error_store).await;

    for v in videos.into_iter().take(10) {
        println!(
            "{}\t{}({})\t{}",
            v.title(),
            v.subscription(),
            v.platform(),
            v.url()
        );
    }
}
