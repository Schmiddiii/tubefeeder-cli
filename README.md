# Tubefeeder-CLI

Integrating [Tubefeeder](tubefeeder.de) into [SXMO](https://sxmo.org/).

## Usage

Compile it `cargo build --release`, throw the resulting binary `target/release/tubefeeder_cli` into `~/.local/bin`, throw the `tubefeeder_cli.sh` into `~/.config/sxmo/userscripts` and run it using the system menu under `Scripts`.

You will also need to install Tubefeeder and subscribe to channels using the application.

## Hard-Coded

Currently hard-coded are:

- The binary location `~/.local/bin/tubefeeder_cli`.
- The player `flatpak run com.github.rafostar.Clapper`.
- The maximum number of videos to print.
